package com.jaspersoft.jasperserver.ps.OIDC;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.security.core.Authentication;

import com.jaspersoft.jasperserver.api.security.externalAuth.ExternalDataSynchronizerImpl;
import com.jaspersoft.jasperserver.api.security.externalAuth.processors.ProcessorData;

public class OIDCExternalDataSynchronizer extends ExternalDataSynchronizerImpl {

  private static Log log = LogFactory.getLog(OIDCExternalDataSynchronizer.class);

  @Override
  protected void loadExternalUserDetailsToProcessorData(Authentication auth) {
      final ProcessorData processorData = ProcessorData.getInstance();
      OIDCUserDetails user = (OIDCUserDetails) auth.getDetails();
      String tenantId = user.getTenantId();
      log.debug("is externally defined: " +  user.isExternallyDefined());
      processorData.addData(ProcessorData.Key.EXTERNAL_AUTH_DETAILS, user);
      processorData.addData(ProcessorData.Key.EXTERNAL_AUTHORITIES, user.getAuthorities());
      processorData.addData(ProcessorData.Key.EXTERNAL_JRS_USER_TENANT_ID, tenantId == null || tenantId.trim().length() == 0 ? null : tenantId);
  }
}
