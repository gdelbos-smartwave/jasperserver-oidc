package com.jaspersoft.jasperserver.ps.OIDC;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.security.authentication.AuthenticationProvider;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.AuthenticationException;
import org.springframework.security.core.userdetails.UserDetails;

public class OIDCAuthenticationProvider implements AuthenticationProvider {

  private static Log log = LogFactory.getLog(OIDCAuthenticationProvider.class);

  private OIDCUserDetailsService userDetailsService;

  @Override
  public Authentication authenticate(Authentication authentication) throws AuthenticationException {
    OIDCAuthenticationToken token = (OIDCAuthenticationToken) authentication;
    log.debug("Calling user details service to validate the access token.");
    UserDetails userDetails = userDetailsService.retrieveUserDetails(token);

    log.debug("Access token validated, returning successfull authentication.");
    OIDCAuthenticationToken response = new OIDCAuthenticationToken(token.getAccessToken(), token.getIdToken(), userDetails.getUsername(), userDetails.getPassword(), userDetails.getAuthorities());
    response.setDetails(userDetails);
    return response;
  }

  @Override
  public boolean supports(Class<?> clazz) {
    log.debug("Checking if " + clazz.getName() + " is supported by OIDCAuthenticationProvider.");
    return OIDCAuthenticationToken.class.isAssignableFrom(clazz);
  }

  public OIDCUserDetailsService getUserDetailsService() {
    return userDetailsService;
  }

  public void setUserDetailsService(OIDCUserDetailsService userDetailsService) {
    this.userDetailsService = userDetailsService;
  }
}
