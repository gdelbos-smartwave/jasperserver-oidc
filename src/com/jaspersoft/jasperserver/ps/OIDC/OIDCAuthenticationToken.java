package com.jaspersoft.jasperserver.ps.OIDC;

import java.util.ArrayList;
import java.util.Collection;

import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.GrantedAuthority;

public class OIDCAuthenticationToken extends UsernamePasswordAuthenticationToken {

  private String accessToken;
  private String idToken;
  private Collection<? extends GrantedAuthority> authorities;

  public OIDCAuthenticationToken(String accessToken, String idToken, Object principal, Object credentials) {
    super(principal, credentials);
    this.accessToken = accessToken;
    this.idToken = idToken;
  }

  public OIDCAuthenticationToken(String accessToken, String idToken, Object principal, Object credentials, Collection<? extends GrantedAuthority> authorities) {
    super(principal, credentials, authorities);
    this.accessToken = accessToken;
    this.idToken = idToken;
    this.authorities = authorities;
  }

  public String getAccessToken() {
    return accessToken;
  }

  public void setAccessToken(String accessToken) {
    this.accessToken = accessToken;
  }

  public String getIdToken() {
    return idToken;
  }

  public void setIdToken(String idToken) {
    this.idToken = idToken;
  }

  @Override
  public Collection<GrantedAuthority> getAuthorities() {
    if (authorities != null) {
      return new ArrayList<>(authorities);
    } else {
      return new ArrayList<>();
    }
  }
}
