package com.jaspersoft.jasperserver.ps.OIDC;

import org.springframework.security.core.AuthenticationException;
import org.springframework.security.core.userdetails.UserDetails;

public interface OIDCUserDetailsService {

  UserDetails retrieveUserDetails(OIDCAuthenticationToken authentication) throws AuthenticationException;
}
