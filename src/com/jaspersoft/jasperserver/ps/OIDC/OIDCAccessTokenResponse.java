package com.jaspersoft.jasperserver.ps.OIDC;

import org.apache.oltu.oauth2.client.response.OAuthAccessTokenResponse;
import org.apache.oltu.oauth2.common.OAuth;
import org.apache.oltu.oauth2.common.exception.OAuthProblemException;
import org.apache.oltu.oauth2.common.token.BasicOAuthToken;
import org.apache.oltu.oauth2.common.token.OAuthToken;
import org.apache.oltu.oauth2.common.utils.JSONUtils;

public class OIDCAccessTokenResponse extends OAuthAccessTokenResponse {

  private String accessToken;
  private String idToken;
  private Long expiresIn;
  private String scopes;
  private String tokenType;

  @Override
  public String getAccessToken() {
    return accessToken;
  }

  public String getIdToken() {
    return idToken;
  }

  @Override
  public Long getExpiresIn() {
    return expiresIn;
  }

  @Override
  public OAuthToken getOAuthToken() {
    return new BasicOAuthToken(getAccessToken(), getTokenType());
  }

  @Override
  public String getRefreshToken() {
    return null;
  }

  @Override
  public String getScope() {
    return scopes;
  }

  @Override
  public String getTokenType() {
    return tokenType;
  }

  @Override
  protected void setContentType(String contentType) {
    this.contentType = contentType;
  }

  @Override
  protected void setResponseCode(int responseCode) {
    this.responseCode = responseCode;
  }

  @Override
  protected void init(String body, String contentType, int responseCode) throws OAuthProblemException {
    this.body = body;
    this.setContentType(contentType);
    this.setResponseCode(responseCode);
    this.parameters = JSONUtils.parseJSON(body);
    
    this.accessToken = getParam(OAuth.OAUTH_ACCESS_TOKEN);
    this.idToken = getParam("id_token");
    this.tokenType = getParam(OAuth.OAUTH_TOKEN_TYPE);
    this.scopes = getParam(OAuth.OAUTH_SCOPE);
    
    String value = getParam(OAuth.OAUTH_EXPIRES_IN);
    this.expiresIn = value == null ? null : Long.valueOf(value);
  }
}
