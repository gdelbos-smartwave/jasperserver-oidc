package com.jaspersoft.jasperserver.ps.OIDC;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Date;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

import org.springframework.security.core.GrantedAuthority;

import com.jaspersoft.jasperserver.api.metadata.user.domain.Role;
import com.jaspersoft.jasperserver.api.metadata.user.domain.User;
import com.jaspersoft.jasperserver.api.security.externalAuth.ExternalUserDetails;

public class OIDCUserDetails extends ExternalUserDetails implements User {

  private Collection<? extends GrantedAuthority> authorities;
  private List<Object> attributes;
  private Set<Role> roles;
  private String fullName;
  private String emailAddress;
  private boolean externallyDefined;
  private Date previousPasswordChangeTime;
  private String tenantId;

  public OIDCUserDetails(String username, String password, boolean enabled, boolean accountNonExpired, boolean credentialsNonExpired, boolean accountNonLocked, Collection<GrantedAuthority> authorities) {
    super(username, password, enabled, accountNonExpired, credentialsNonExpired, accountNonLocked, authorities);
    this.authorities = authorities;
  }

  @Override
  public Collection<GrantedAuthority> getAuthorities() {
    if (authorities != null) {
      return new ArrayList<>(authorities);
    } else {
      return new ArrayList<>();
    }
  }

  @Override
  public void setUsername(String username) {
  }

  @Override
  public void setPassword(String password) {
  }

  @Override
  public void setEnabled(boolean enabled) {
  }

  @Override
  public List<Object> getAttributes() {
    return attributes;
  }

  @SuppressWarnings({ "unchecked", "rawtypes" })
  @Override
  public void setAttributes(List attributes) {
    this.attributes = new ArrayList<>(attributes);
  }

  @Override
  public Set<Role> getRoles() {
    return roles;
  }

  @SuppressWarnings({ "unchecked", "rawtypes" })
  @Override
  public void setRoles(Set roles) {
    this.roles = new HashSet<>(roles);
  }

  @Override
  public void addRole(Role aRole) {
    this.roles.add(aRole);
  }

  @Override
  public void removeRole(Role aRole) {
    this.roles.remove(aRole);
  }

  @Override
  public String getFullName() {
    return this.fullName;
  }

  @Override
  public void setFullName(String fullName) {
    this.fullName = fullName;
  }

  @Override
  public String getEmailAddress() {
    return this.emailAddress;
  }

  @Override
  public void setEmailAddress(String emailAddress) {
    this.emailAddress = emailAddress;
  }

  @Override
  public boolean isExternallyDefined() {
    return externallyDefined;
  }

  @Override
  public void setExternallyDefined(boolean externallyDefined) {
    this.externallyDefined = externallyDefined;
  }

  @Override
  public Date getPreviousPasswordChangeTime() {
    return previousPasswordChangeTime;
  }

  @Override
  public void setPreviousPasswordChangeTime(Date previousPasswordChangeTime) {
    this.previousPasswordChangeTime = previousPasswordChangeTime;
  }

  @Override
  public String getTenantId() {
    return this.tenantId;
  }

  @Override
  public void setTenantId(String tenantId) {
    this.tenantId = tenantId;
  }
}
