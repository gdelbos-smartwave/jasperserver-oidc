package com.jaspersoft.jasperserver.ps.OIDC;

import java.net.URL;
import java.security.Key;
import java.security.PublicKey;
import java.text.ParseException;
import java.util.ArrayList;
import java.util.LinkedList;
import java.util.List;
import java.util.Locale;
import java.util.Objects;

import javax.crypto.SecretKey;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.security.authentication.AuthenticationServiceException;
import org.springframework.security.authentication.BadCredentialsException;
import org.springframework.security.authentication.InsufficientAuthenticationException;
import org.springframework.security.core.AuthenticationException;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.authority.SimpleGrantedAuthority;
import org.springframework.security.core.userdetails.UserDetails;

import com.nimbusds.jose.JOSEException;
import com.nimbusds.jose.JWSHeader;
import com.nimbusds.jose.KeySourceException;
import com.nimbusds.jose.jwk.JWK;
import com.nimbusds.jose.jwk.JWKMatcher;
import com.nimbusds.jose.jwk.JWKSelector;
import com.nimbusds.jose.jwk.KeyConverter;
import com.nimbusds.jose.jwk.source.JWKSource;
import com.nimbusds.jose.jwk.source.RemoteJWKSet;
import com.nimbusds.jose.proc.BadJOSEException;
import com.nimbusds.jose.proc.JWSKeySelector;
import com.nimbusds.jose.proc.SecurityContext;
import com.nimbusds.jwt.JWTClaimsSet;
import com.nimbusds.jwt.proc.ConfigurableJWTProcessor;
import com.nimbusds.jwt.proc.DefaultJWTClaimsVerifier;
import com.nimbusds.jwt.proc.DefaultJWTProcessor;

public class OIDCUserDetailsServiceImpl implements OIDCUserDetailsService {

  private static Log log = LogFactory.getLog(OIDCUserDetailsServiceImpl.class);

  private String issuer;
  private String audience;
  private String rolesClaim;
  private String clientId;
  private URL jwksUrl;

  @Override
  public UserDetails retrieveUserDetails(OIDCAuthenticationToken authentication) throws AuthenticationException {
    JWKSource<SecurityContext> keySource = new RemoteJWKSet<>(jwksUrl);

    ConfigurableJWTProcessor<SecurityContext> processor = new DefaultJWTProcessor<>();
    processor.setJWSKeySelector(new JWSCommonKeySelector<>(keySource));
    processor.setJWTClaimsSetVerifier(new DefaultJWTClaimsVerifier<>());
    JWTClaimsSet claims;
    try {
      claims = processor.process(authentication.getIdToken(), null);
      log.debug(claims);
      if (claims.getAudience() == null || !claims.getAudience().contains(audience)) {
        String errorMessage = "Claim 'aud' is missing or does not contain [" + audience + "]";
        log.error(errorMessage);
        throw new InsufficientAuthenticationException(errorMessage);
      }
      if (claims.getIssuer() == null || !Objects.equals(claims.getIssuer(), issuer)) {
        String errorMessage = "Claim 'iss' is missing or does not contain [" + issuer + "]";
        log.error(errorMessage);
        throw new InsufficientAuthenticationException(errorMessage);
      }
    } catch (ParseException | BadJOSEException | JOSEException e) {
      log.error("Error while validating access token", e);
      throw new BadCredentialsException("Error while validating access token", e);
    }

    try {
      List<String> roles = claims.getStringListClaim(rolesClaim);
      List<GrantedAuthority> grantedAuthorities = new ArrayList<>();
      roles.forEach(r -> grantedAuthorities.add(new SimpleGrantedAuthority("ROLE_" + r.toUpperCase(Locale.getDefault()))));

      OIDCUserDetails user = new OIDCUserDetails(
        claims.getStringClaim("preferred_username"),
        "4N3v3R6u3s5",
        true,
        true,
        true,
        true,
        grantedAuthorities
      );
      user.setExternallyDefined(true);
      user.setEmailAddress(claims.getStringClaim("email"));
      user.setFullName(claims.getStringClaim("name"));
      return user;
    } catch (ParseException e) {
      log.error("Error while validating access token", e);
      throw new AuthenticationServiceException("Error while retrieving user claims", e);
    }
  }

  public String getIssuer() {
    return issuer;
  }

  public void setIssuer(String issuer) {
    this.issuer = issuer;
  }

  public String getAudience() {
    return audience;
  }

  public void setAudience(String audience) {
    this.audience = audience;
  }

  public String getRolesClaim() {
    return rolesClaim;
  }

  public void setRolesClaim(String rolesClaim) {
    this.rolesClaim = rolesClaim;
  }

  public String getClientId() {
    return clientId;
  }

  public void setClientId(String clientId) {
    this.clientId = clientId;
  }

  public URL getJwksUrl() {
    return jwksUrl;
  }

  public void setJwksUrl(URL jwksUrl) {
    this.jwksUrl = jwksUrl;
  }

  public static class JWSCommonKeySelector<C extends SecurityContext> implements JWSKeySelector<C> {

    private final JWKSource<C> source;

    public JWSCommonKeySelector(JWKSource<C> source) {
      this.source = source;
    }

    @Override
    public List<Key> selectJWSKeys(JWSHeader jwsHeader, C context) throws KeySourceException {
      JWKMatcher jwkMatcher = JWKMatcher.forJWSHeader(jwsHeader);
      List<JWK> jwkMatches = source.get(new JWKSelector(jwkMatcher), context);
      List<Key> sanitizedKeyList = new LinkedList<>();

      for (Key key : KeyConverter.toJavaKeys(jwkMatches)) {
        if (key instanceof PublicKey || key instanceof SecretKey) {
          sanitizedKeyList.add(key);
        }
      }

      return sanitizedKeyList;
    }
  }
}
