package com.jaspersoft.jasperserver.ps.OIDC;

import java.io.IOException;
import java.util.HashMap;
import java.util.Map;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.commons.codec.binary.Base64;
import org.apache.commons.lang3.StringUtils;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.apache.oltu.oauth2.client.OAuthClient;
import org.apache.oltu.oauth2.client.URLConnectionClient;
import org.apache.oltu.oauth2.client.request.OAuthClientRequest;
import org.apache.oltu.oauth2.client.request.OAuthClientRequest.TokenRequestBuilder;
import org.apache.oltu.oauth2.common.exception.OAuthProblemException;
import org.apache.oltu.oauth2.common.exception.OAuthSystemException;
import org.apache.oltu.oauth2.common.message.types.GrantType;
import org.springframework.security.authentication.AuthenticationServiceException;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.AuthenticationException;
import org.springframework.security.web.authentication.AbstractAuthenticationProcessingFilter;

public class OIDCPreAuthenticationFilter extends AbstractAuthenticationProcessingFilter {

  private static Log log = LogFactory.getLog(OIDCPreAuthenticationFilter.class);

  private static final String TOKEN_AUTHENTICATION_HEADER = "Header";
  private static final String TOKEN_AUTHENTICATION_BODY = "Body";

  private String authorizeEndpoint;
  private String tokenEndpoint;
  private String tokenAuthentication;
  private String clientId;
  private String clientSecret;
  private String scopes;

  public OIDCPreAuthenticationFilter() {
    super("/oidc");
    setContinueChainBeforeSuccessfulAuthentication(false);
  }

  @Override
  public Authentication attemptAuthentication(HttpServletRequest request, HttpServletResponse response) throws AuthenticationException, IOException, ServletException {
    String code = request.getParameter("code");
    log.info("Going to use [" + request.getRequestURL().toString() + "] as redirect URI");
    if (StringUtils.isNotBlank(code)) {
      log.debug("Request contains a 'code' parameter. Calling token endpoint for token exchange");
      return exchangeCodeForToken(request, response, code);
    } else {
      log.debug("Request does not contain the 'code' parameter. Redirection to authorize endpoint");
      return redirectToAuthorize(request, response);
    }
  }

  private Authentication exchangeCodeForToken(HttpServletRequest req, HttpServletResponse resp, String code) throws AuthenticationException, IOException, ServletException {
    OAuthClient client = new OAuthClient(new URLConnectionClient());
    try {
      TokenRequestBuilder builder = OAuthClientRequest
          .tokenLocation(tokenEndpoint)
          .setGrantType(GrantType.AUTHORIZATION_CODE)
          .setClientId(clientId)
          .setCode(code);
      if (TOKEN_AUTHENTICATION_BODY.equals(tokenAuthentication)) {
        builder
            .setClientSecret(clientSecret)
            .setRedirectURI(req.getRequestURL().toString());
      }
      OAuthClientRequest oauthClientRequest = builder.buildBodyMessage();

      if (TOKEN_AUTHENTICATION_HEADER.equals(tokenAuthentication)) {
        oauthClientRequest.setHeaders(getBasicAuthorizationHeader(clientId, clientSecret));
      }

      OIDCAccessTokenResponse oauthResponse = client.accessToken(oauthClientRequest, OIDCAccessTokenResponse.class);
      OIDCAuthenticationToken token = new OIDCAuthenticationToken(oauthResponse.getAccessToken(), oauthResponse.getIdToken(), oauthResponse.getIdToken(), null);
      return getAuthenticationManager().authenticate(token);
    } catch (OAuthSystemException | OAuthProblemException e) {
      log.error("An error occurs while exchanging OAuth authorization code for access token.", e);
      throw new AuthenticationServiceException("Error while preparing the OAuth system.", e);
    }
  }

  private static Map<String, String> getBasicAuthorizationHeader(String key, String secret) {
    Map<String, String> headers = new HashMap<String, String>();
    String auth = key + ":" + secret;
    byte[] encodedAuth = Base64.encodeBase64(auth.getBytes());
    headers.put("Authorization", "Basic " + new String(encodedAuth));
    return headers;
  }

  private Authentication redirectToAuthorize(HttpServletRequest req, HttpServletResponse resp) throws IOException, ServletException {
    try {
      OAuthClientRequest oauthClientRequest = OAuthClientRequest
          .authorizationLocation(authorizeEndpoint)
          .setResponseType("code")
          .setClientId(clientId)
          .setScope(scopes)
          .setRedirectURI(req.getRequestURL().toString())
          .buildQueryMessage();

      resp.sendRedirect(oauthClientRequest.getLocationUri());
      return null;
    } catch (OAuthSystemException e) {
      log.error("An error occurs while initiating OAuth authorization request.", e);
      throw new AuthenticationServiceException("Error while preparing the OAuth system.", e);
    }
  }

  @Override
  public void destroy() {
    // Nothing
  }

  public void setAuthorizeEndpoint(String authorizeEndpoint) {
    this.authorizeEndpoint = authorizeEndpoint;
  }

  public void setTokenEndpoint(String tokenEndpoint) {
    this.tokenEndpoint = tokenEndpoint;
  }

  public void setTokenAuthentication(String tokenAuthentication) {
    this.tokenAuthentication = tokenAuthentication;
  }

  public void setClientId(String clientId) {
    this.clientId = clientId;
  }

  public void setClientSecret(String clientSecret) {
    this.clientSecret = clientSecret;
  }

  public void setScopes(String scopes) {
    this.scopes = scopes;
  }
}
