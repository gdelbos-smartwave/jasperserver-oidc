# JasperServer plugin for OIDC authentication

La version 7 de JasperServer ne permet pas la délégation de l'authentification à un server en utilisant le protocole OIDC. Ce module est là pour permettre cette authentification.

# Installation

## Compilation

Le projet se base sur le classpath proposé par Tomcat et JasperServer. De plus, pour les besoin de ce plugin, deux librairies externes doivent être ajoutée (disponible dans le dossier `lib`).

Il est donc nécessaire, pour compiler le projet et créer l'archive JAR d'avoir dans le classpath les éléments suivants:
- ${TOMCAT_INSTALLATION_PATH}/lib/*.jar
- ${TOMCAT_INSTALLATION_PATH}/webapps/jasperserver/WEB-INF/lib/*.jar
- ${PROJECT_PATH}/lib/*.jar

## Création du JAR

L'archive JAR doit être créée à partir des sources compilées. Seuls les fichiers `.class` sont attendu.  
Les fichiers resources (`applicationContext*.xml`) n'ont pas besoin d'être dans l'archive.

## Déploiement

Le déploiement s'effectue de la façon suivante:
- Copie des deux librairies `` et `` (présente dans le dossier `lib`) vers `${TOMCAT_INSTALLATION_PATH}/webapps/jasperserver/WEB-INF/lib`
- Copie de l'archive JAR créée précédemment vers `${TOMCAT_INSTALLATION_PATH}/webapps/jasperserver/WEB-INF/lib`

# Configuration

## Keycloak

La configuration de Keycloak comprend:
- La création d'un client
- L'ajout d'un mapping de claim

### Création du client

TODO

### Mapping du claim

TODO

## JasperServer

La configuration est portée par le fichier `applicationContext-externalAuth-oidc.xml`.  
Dans ce fichier, il est nécessaire de modifier les placeholders selon les informations suivantes:

| Placeholder | Description | Exemple |
| --- | --- | --- |
| `${OAUTH_CLIENT_ID}` | Client ID configuré pour l'authentification | `jasperserver` |
| `${OAUTH_CLIENT_SECRET}` | Client Secret associé | `1fwY9ymzaebguzkMZQKVKzehyIyhgGxt` |
| `${OAUTH_ISSUER}` | Le fournisseur de jeton (`issuer`) | `http://localhost:8080/auth/realms/custom` |
| `${OAUTH_AUTHORIZATION_URL}` | L'URL d'autorisation (`authorization_endpoint`) | `http://localhost:8080/auth/realms/custom/protocol/openid-connect/auth` |
| `${OAUTH_TOKEN_URL}` | L'URL de jeton (`token_endpoint`) | `http://localhost:8080/auth/realms/custom/protocol/openid-connect/token` |
| `${OAUTH_JWKS_URL}` | L'URL de jeton (`jwks_url`) | `http://localhost:8080/auth/realms/custom/protocol/openid-connect/certs` |


De plus, il est nécessaire d'effectuer le mapping entre les rôles remontant de Keycloak et les rôles attendu dans Jasper.  
Dans Keycloak, les rôles sont des noms en minuscule (ex: `bger_informatik_alle`). 
Ces rôles sont transformer en `Authority` en respectant la règle suivante:
- Mise en majuscule
- Prefix avec la chaine de caractère `ROLE_`

Le mapping de ces rôle se fait dans la partie commenter du fichier en respectant le format suivant:
- Un mapping est une `entry`
- Chaque `entry` contient une `key` correspondant au rôle provenant de Keycloak après transformation (ex: `ROLE_BGER_INFORMATIK_ALLE`)
- Chaque `entry` contient une `value` correspondant au rôle dans JasperServer.

Exemple:
```xml
        <entry>
          <key>
            <value>ROLE_BGER_INFORMATIK_ALLE</value>
          </key>
          <value>ROLE_GS</value>
        </entry>
```

